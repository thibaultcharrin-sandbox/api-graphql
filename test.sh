#!/bin/bash

function do_curl() {
    curl 127.0.0.1:8080/graphql -H 'Content-Type: Application/JSON' -d '{"query": "{allPeople{NAME}}"}'
}

if [[ $(command -v lolcat) ]]; then
    do_curl | lolcat
else
    do_curl
fi
