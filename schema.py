import graphene
from graphene.relay import Node
from graphene.types.scalars import Scalar
from graphene_mongo import MongoengineObjectType

from models import Address, Person, Other


# Custom GraphQL Scalar to handle dynamic keys in PersonType
class DynamicDataScalar(Scalar):
    @staticmethod
    def serialize(data):
        return data

    @staticmethod
    def parse_value(data):
        return data

    @staticmethod
    def parse_literal(ast):
        return ast.value


# Define the GraphQL types for Address and Person
class OtherType(MongoengineObjectType):
    class Meta:
        model = Other
        interfaces = (Node,)


class AddressType(MongoengineObjectType):
    class Meta:
        model = Address
        interfaces = (Node,)


class PersonType(MongoengineObjectType):
    class Meta:
        model = Person
        interfaces = (Node,)

    dynamic_data = graphene.Field(DynamicDataScalar)

    def resolve_dynamic_data(parent, info):
        return parent.dynamic_data


# Custom input type for dynamic data filtering
class DictFilter(graphene.InputObjectType):
    key = graphene.String(required=True)
    value = graphene.String(required=True)
    last = graphene.Boolean()


# Define the query for fetching people
class Query(graphene.ObjectType):
    person = Node.Field(PersonType)
    all_people = graphene.List(PersonType, filter=graphene.List(DictFilter))

    def resolve_all_people(self, info, filter=None):
        query = Person.objects

        if filter:
            for filter_item in filter:
                if filter_item.key == "NAME" and filter_item.last:
                    max_age = (
                        query.filter(NAME=filter_item.value)
                        .order_by("-age")
                        .first()
                        .age
                    )
                    query = query.filter(NAME=filter_item.value, age=max_age)
                elif (
                    "/" in filter_item.key
                ):  # Handle filtering on embedded document field
                    nested_key_parts = filter_item.key.split("/")
                    nested_key = "__".join(nested_key_parts)
                    query = query.filter(**{nested_key: filter_item.value})
                else:
                    query = query.filter(**{filter_item.key: filter_item.value})

        return query


schema = graphene.Schema(query=Query, types=[PersonType])
