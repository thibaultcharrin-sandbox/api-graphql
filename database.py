from mongoengine import connect
from models import Person, Address, Other

client = connect(
    "my_database", host="mongodb://myuser:mypwd@mongo:27017/", alias="default"
)


def init_db():
    client.drop_database("my_database")

    # Create some sample other documents
    other1 = Other(comment="Fantastic it is working")
    other2 = Other(comment="Beautiful weather")

    # Create some sample addresses
    address1 = Address(
        street="123 Main St", city="New York", state="NY", country="USA", other=other1
    )
    address2 = Address(
        street="456 Elm St",
        city="San Francisco",
        state="CA",
        country="USA",
        other=other2,
    )

    # Create some sample people
    person1 = Person(
        NAME="John Doe",
        age="30",
        email="johndoe@example.com",
        address=address1,
        dynamic_data={
            "tutu": {
                "version": {
                    "1": "truc",
                    "2": "truc",
                },
                "versions": ["1", "2"],
            }
        },
    )
    person2 = Person(
        NAME="Tango Charlie",
        age="29",
        email="tc@example.com",
        address=address2,
        dynamic_data={
            "tata": {
                "version": {
                    "3": "truc",
                    "4": "truc",
                },
                "versions": ["3", "4"],
            }
        },
    )
    person3 = Person(
        NAME="John Doe",
        age="35",
        email="potato@potato.potato",
        address=address1,
        dynamic_data={
            "tutu": {
                "version": {
                    "1": "truc",
                    "2": "truc",
                },
                "versions": ["1", "2"],
            }
        },
    )

    # Save people to the database
    person1.save()
    person2.save()
    person3.save()
