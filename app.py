from flask import Flask, request
from graphql_server.flask import GraphQLView
from database import init_db
from schema import schema


app = Flask(__name__)
app.url_map.strict_slashes = False

DEFAULT_QUERY = """
{
  allPeople(
    filter: [
      {key: "dynamic_data/tutu/version/1", value: "truc"},
      {key: "NAME", value: "John Doe", last: true}]
  ) {
    NAME
    email
    age
    address {
      street
      city
      state
      country
      other {
        comment
      }
    }
    dynamicData
  }
}
""".strip().replace(
    "\n", ""
)


class GraphQLView(GraphQLView):
    def get_context(self):
        try:
            my_query = (
                request.get_json().get("query").replace("\n", "").replace(" ", "")
            )
            app.logger.debug(f"{my_query=}")
        except:
            app.logger.debug(f"{request=}")
        return request

    def get_validation_rules(self):
        return self.validation_rules


@app.route("/")
def ping():
    return {"status": "healthy"}


app.add_url_rule(
    "/graphql",
    view_func=GraphQLView.as_view(
        "graphql",
        schema=schema,
        pretty=True,
        graphiql=True,
        graphiql_version="3.0.0",
        grpahiql_html_title="GraphQL IDE",
        default_query=DEFAULT_QUERY,
    ),
)


if __name__ == "__main__":
    init_db()
    app.run(debug=True, host="0.0.0.0", port=8080)
