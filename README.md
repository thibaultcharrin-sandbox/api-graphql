<div align="center">
    <img src="logo.png" alt="GraphQL Logo" width="256" />
    <h1>GraphQL-Server for MongoDB using Python Flask</h1>
    <p>Arguably the simplest Python implementation of a GraphQL Server for a MongoDB database.</p>
    <img src="screen.png" width="824" alt="Screenshot"/>
</div>

# Wiki

## app.py

This component is responsible for setting up and running the Flask application for the GraphQL server. It imports necessary modules, initializes the Flask app, configures routes and endpoints, and starts the server.

The key responsibilities of `app.py` include:
- Setting up Flask application and routes
- Initializing the GraphQL view for handling GraphQL queries
- Configuring the app to handle health checks and other endpoints
- Running the Flask application server

## database.py

The `database.py` component is responsible for managing the connection and initialization of the MongoDB database. It sets up the connection to the MongoDB server and provides functions for initializing the database and performing any necessary setup or cleanup tasks.

The key responsibilities of `database.py` include:
- Establishing a connection to the MongoDB server
- Initializing the database and performing any necessary setup tasks
- Providing functions to interact with the database and perform database operations
- Managing database connections and cleanup

## models.py

The `models.py` component defines the data models or schemas used in the GraphQL server. It utilizes an object-document mapping (ODM) library, such as mongoengine, to define the structure and behavior of the MongoDB documents.

The key responsibilities of `models.py` include:
- Defining classes that represent the MongoDB document schemas
- Specifying the fields, data types, and relationships of each document
- Defining any embedded documents or references to other documents
- Providing metadata and options for interacting with the MongoDB collection

## schema.py

The `schema.py` component defines the GraphQL schema for the GraphQL server. It utilizes a GraphQL library, such as Graphene, to define the types, queries, mutations, and resolvers that make up the GraphQL API.

The key responsibilities of `schema.py` include:
- Defining the GraphQL types that correspond to the MongoDB document models
- Specifying the queries and mutations that can be performed on the data
- Implementing resolvers to fetch and manipulate data from the MongoDB database
- Configuring the GraphQL schema and defining any additional custom types or scalars

## You are all set!

Enjoy your new GraphQL Server. Spread the word, give this project a star, and feel free to reach out for suggestions.

# Credits

- [Thibault Charrin](https://gitlab.com/thibaultcharrin) (_DevOps | Full Stack Developper_)
    
    - Main author, script implementation
