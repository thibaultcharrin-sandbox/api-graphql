from mongoengine import (
    EmbeddedDocument,
    StringField,
    Document,
    EmbeddedDocumentField,
    DictField,
)


# Define an EmbeddedDocument class representing other
class Other(EmbeddedDocument):
    comment = StringField()


# Define an EmbeddedDocument class representing an address
class Address(EmbeddedDocument):
    street = StringField(required=True, max_length=100)
    city = StringField(required=True, max_length=50)
    state = StringField(max_length=50)
    country = StringField(max_length=50)
    other = EmbeddedDocumentField(Other)


# Define a Document class representing a person
class Person(Document):
    NAME = StringField(required=True, max_length=50)
    age = StringField(required=True)
    email = StringField(max_length=100)
    address = EmbeddedDocumentField(Address)
    dynamic_data = DictField()

    meta = {"collection": "people"}  # Specify the collection name
